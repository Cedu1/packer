packer {
  required_plugins {
    virtualbox = {
      version = ">= 0.0.1"
      source = "github.com/hashicorp/virtualbox"
    }
    windows-update = {
          version = "0.12.0"
          source = "github.com/rgl/windows-update"
        }
    }
}
source "virtualbox-iso" "Windows10Pro"{

 guest_os_type = "Windows10_64"
 guest_additions_mode = "attach"
 iso_url          = "C:/Users/CED-Admin/Desktop/Windows.iso"
 iso_checksum     = "md5:3ccf3174b07fe9d193975c3ee7415443" #Checksum of your ISO, Packer returns the exepted value after failing to run. Else use File Checksum Integrity Verifier (FCIV)
 #output_directory = "Windows10ProBox"
 disk_size        = "50000"
 communicator     = "winrm"
 winrm_username   = "vagrant"
 winrm_password   = "vagrant"
 winrm_timeout    = "20m"
 vm_name        = "Cli-Windows10Pro"

 #Min Requierements 512MB RAM, 10Mb GPU Full Screen Mode
 vboxmanage = [
   ["modifyvm", "{{.Name}}", "--memory", "4096"],
   ["modifyvm", "{{.Name}}", "--cpus", "2"],
   ["modifyvm", "{{.Name}}", "--vram", "100"]
 ]

floppy_files   = ["C:/Users/CED-Admin/Documents/NewVagrantImage/Windows10Pro/autounattend.xml",
                 "C:/Users/CED-Admin/Documents/NewVagrantImage/Windows10Pro/scripts/enablewinrm.ps1",
                 "C:/Users/CED-Admin/Documents/NewVagrantImage/Windows10Pro/scripts/enablerdp.bat"]

shutdown_command = "shutdown /s /t 10 /f /d p:4:1"
}

build {
 source "virtualbox-iso.Windows10Pro" {}

provisioner "windows-update"{
   search_criteria = "IsInstalled=0"
   filters = [
  "exclude:$_.Title -like '*Preview*'",
   "include:$true"]
   update_limit = 30
   }

 post-processors {
 post-processor "vagrant" {
   keep_input_artifact = false
   provider_override = "virtualbox"
   output = "Windows10Pro.box"
   }

   post-processor "vagrant-cloud" {
      access_token = "Ra2gMgHKkWc4Kg.atlasv1.kEmzCIO72kFwA9BKvoVPrTElbhm3oSHBfSrXzO0OwxSMIP3dSq3YyjqwwOWN4SjGs64"
      box_tag      = "CedricBau/Windows10Pro"
     version      = "0.5"
     version_description = "Guest Addittons installed. No automated installation during production of box able yet. Needs to be installed manually during launch of packer config file."
    }
 }
}
