packer {
  required_plugins {
    virtualbox = {
      version = ">= 0.0.1"
      source = "github.com/hashicorp/virtualbox"
    }
    windows-update = {
          version = "0.12.0"
          source = "github.com/rgl/windows-update"
        }
  }
}

source "virtualbox-iso" "WindowsServer19"{

 guest_os_type = "Windows-server-2019"
 guest_additions_mode = "attach"
 iso_url          = "https://software-download.microsoft.com/download/pr/17763.737.190906-2324.rs5_release_svc_refresh_SERVER_EVAL_x64FRE_en-us_1.iso"
 iso_checksum     = "sha256:549bca46c055157291be6c22a3aaaed8330e78ef4382c99ee82c896426a1cee1" #Checksum of your ISO, Packer returns the exepted value after failing to run. Else use File Checksum Integrity Verifier (FCIV)
 output_directory = "WindowsServer2019"
 disk_size        = "50000"
 communicator     = "winrm"
 winrm_username   = "vagrant"
 winrm_password   = "vagrant"
 winrm_timeout    = "30m"
 winrm_insecure   = true

 vboxmanage = [
   ["modifyvm", "{{.Name}}", "--memory", "4096"],
   ["modifyvm", "{{.Name}}", "--cpus", "2"],
   ["modifyvm", "{{.Name}}", "--vram", "100"]
 ]

floppy_files   = ["C:/Users/CED-Admin/Documents/NewVagrantImage/WinServer19/autounattend.xml",
                 "C:/Users/CED-Admin/Documents/NewVagrantImage/WinServer19/scripts/enablewinrm.ps1",
                 "C:/Users/CED-Admin/Documents/NewVagrantImage/WinServer19/scripts/fixnetwork.ps1",
                 "C:/Users/CED-Admin/Documents/NewVagrantImage/WinServer19/scripts/enablerdp.bat"]

shutdown_command = "shutdown /s /t 10 /f /d p:4:1"
}

build {
 source "virtualbox-iso.WindowsServer19" {}

provisioner "windows-update"{
    search_criteria = "IsInstalled=0"
     filters = [
      "exclude:$_.Title -like '*Preview*'",
      "include:$true",
    ]
    update_limit = 25
      }

 post-processors {
 post-processor "vagrant" {
   keep_input_artifact = true
   provider_override   = "virtualbox"
   output = "WindowsServer19.box"
 }
 post-processor "vagrant-cloud" {
    access_token = "Ra2gMgHKkWc4Kg.atlasv1.kEmzCIO72kFwA9BKvoVPrTElbhm3oSHBfSrXzO0OwxSMIP3dSq3YyjqwwOWN4SjGs64"
    box_tag      = "CedricBau/WindowsServer2019"
    version      = "0.3"
    version_description = "Fixed autologon Issues. Guest Addition still not working"
  }
 }
}
