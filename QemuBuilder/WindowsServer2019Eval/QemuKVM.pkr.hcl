#Necessary Plugins

source "qemu" "kvm-standard"{

accelerator         = "kvm"
#boot_command        = ""
boot_wait           = "10s"
communicator        = "winrm"
#disk_interface      = ""
disk_size           = "50000"
format              = "qcow2"
headless            = false #headless decides whether a GUI for the building of the VM should be started or not. If its set to true, no GUI will appear.
http_content        = ""
iso_checksum        = "sha256:549bca46c055157291be6c22a3aaaed8330e78ef4382c99ee82c896426a1cee1"
iso_url             = "https://software-download.microsoft.com/download/pr/17763.737.190906-2324.rs5_release_svc_refresh_SERVER_EVAL_x64FRE_en-us_1.iso"
#net_device          = ""
output_directory    = "qemuVM"
shutdown_command    = "shutdown /s /t 10 /f /d p:4:1"
#shutdown_timeout    = ""
winrm_password      = "vagrant"
winrm_username      = "vagrant"
winrm_timeout       = "20m"
winrm_insecure      = true

#qemuargs = [            #Specification of RAM, CPU of VM

#]

floppy_files   = ["C:/Users/CED-Admin/Documents/NewVagrantImage/WinServer19/autounattend.xml",
                 "C:/Users/CED-Admin/Documents/NewVagrantImage/WinServer19/scripts/enablewinrm.ps1",
                 "C:/Users/CED-Admin/Documents/NewVagrantImage/WinServer19/scripts/enablerdp.bat"]
}

source "virtualbox-iso" "WindowsServer19"{

 guest_os_type      = "Windows-server-2019"
 guest_additions_mode = "attach"
 iso_url            = "https://software-download.microsoft.com/download/pr/17763.737.190906-2324.rs5_release_svc_refresh_SERVER_EVAL_x64FRE_en-us_1.iso"
 iso_checksum       = "sha256:549bca46c055157291be6c22a3aaaed8330e78ef4382c99ee82c896426a1cee1" #Checksum of your ISO, Packer returns the exepted value after failing to run. Else use File Checksum Integrity Verifier (FCIV)
 output_directory   = "WindowsServer2019"
 disk_size          = "50000"
 communicator       = "winrm"
 winrm_username     = "vagrant"
 winrm_password     = "vagrant"
 winrm_timeout      = "30m"
 winrm_insecure     = true

 vboxmanage = [
   ["modifyvm", "{{.Name}}", "--memory", "4096"],
   ["modifyvm", "{{.Name}}", "--cpus", "2"],
   ["modifyvm", "{{.Name}}", "--vram", "100"]
 ]

floppy_files   = ["C:/Users/CED-Admin/Documents/NewVagrantImage/WinServer19/autounattend.xml",
                 "C:/Users/CED-Admin/Documents/NewVagrantImage/WinServer19/scripts/enablewinrm.ps1",
                 "C:/Users/CED-Admin/Documents/NewVagrantImage/WinServer19/scripts/fixnetwork.ps1",
                 "C:/Users/CED-Admin/Documents/NewVagrantImage/WinServer19/scripts/enablerdp.bat"]

shutdown_command = "shutdown /s /t 10 /f /d p:4:1"
}


build {
source "qemu.kvm-standard"{
}

post-processors {
post-processor "vagrant" {
  keep_input_artifact = false
  provider_override = "virtualbox"
  output = "Windows10gPro.box"
  }
}



}
