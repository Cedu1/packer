packer{
required_plugins {
qemu = {
version = ">= 1.0.1"
source = "github.com/hashicorp/qemu"
}
}
}

source "qemu" "kvm-standard"{

accelerator         = "kvm"
boot_command        = ["<spacebar><wait90><return>"]
boot_wait           = "10s"
communicator        = "winrm"
disk_interface      = "ide"
disk_size           = "50000"
format              = "qcow2"
headless            = false #headless decides whether a GUI for the building of the VM should be started or not. If its set to true, no GUI will appear.
iso_checksum        = "sha256:549bca46c055157291be6c22a3aaaed8330e78ef4382c99ee82c896426a1cee1"
iso_url             = "https://software-download.microsoft.com/download/pr/17763.737.190906-2324.rs5_release_svc_refresh_SERVER_EVAL_x64FRE_de-de_1.iso"
net_device          = "e1000"
output_directory    = "qemuVM"
shutdown_command    = "shutdown /s /t 10 /f /d p:4:1"
shutdown_timeout    = "10s"
winrm_password      = "vagrant"
winrm_username      = "vagrant"
winrm_timeout       = "20m"
winrm_insecure      = true

qemuargs = [
["-m", "4096M"],
["-smp", "2"],
["-global", "virtio-blk-pci.physical_block_size=4096"]
]


floppy_files   = ["./autounattend.xml",
                 "./enablewinrm.ps1",
                 "./enablerdp.bat"]

}

build {
source "qemu.kvm-standard"{}

post-processors {
post-processor "vagrant" {
  keep_input_artifact = false
  provider_override = "virtualbox"
  output = "Windows10gPro.box"
  }
}



}
